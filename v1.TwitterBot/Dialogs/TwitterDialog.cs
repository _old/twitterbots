﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;
using System.Net.Http;
using HtmlAgilityPack;

namespace v1.TwitterBot.Dialogs
{
    [Serializable]
    public class TwitterDialog : IDialog<object>
    {
        public Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);

            return Task.CompletedTask;
        }

        private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<object> result)
        {
            var activity = await result as Activity;
            var message = activity.Text;

            //var url = new Uri(message);

            bool isUrl = Uri.TryCreate(message, UriKind.Absolute, out Uri uriResult)
    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (!isUrl)
            {
                await context.PostAsync("Your request is not an url. Post another.");
                await Task.CompletedTask;
            }
            else
            {
                var client = new HttpClient();
                var document = client.GetStringAsync(uriResult).Result;

                var html = new HtmlDocument();
                html.LoadHtml(document);
                var root = html.DocumentNode;
                var descendants = root.Descendants().Where(n => n.GetAttributeValue("class", "").TrimEnd().Equals("AdaptiveMedia-photoContainer js-adaptive-photo"));
                var resources = new List<string>();
                foreach (var descendant in descendants)
                {
                    var imgTag = descendant.Descendants("img").Single();
                    var content = imgTag.GetAttributeValue("src", null);
                    resources.Add(content);
                }
                foreach (var item in resources)
                {
                    await context.PostAsync(item);
                }
                await Task.CompletedTask;
            }
        }
    }
}